// Source:
// - https://gitlab.com/gitlab-org/gitlab/-/blob/d1fcb9ab1911b2b41a68cddb73b8ded276f2cc06/app/assets/stylesheets/highlight/themes/dark.scss#L295
// - https://github.com/mozmorris/tomorrow-pygments/blob/c6ca1a308e7e93cb18a54f93bf964f56e4d07acf/css/tomorrow_night.css
// - https://github.com/mozmorris/tomorrow-pygments/blob/c6ca1a308e7e93cb18a54f93bf964f56e4d07acf/css/tomorrow_night.css#L2
// - https://gitlab.com/gitlab-org/gitlab/-/blob/d1fcb9ab1911b2b41a68cddb73b8ded276f2cc06/app/assets/stylesheets/highlight/themes/dark.scss#L15
// - https://gitlab.com/gitlab-org/gitlab/-/blob/d1fcb9ab1911b2b41a68cddb73b8ded276f2cc06/app/assets/stylesheets/highlight/themes/dark.scss#L42
export const tomorrowNight = {
  name: "tomorrow-night",
  type: "dark",
  settings: [
    {
      settings: {
        foreground: "#c5c8c6",
        background: "#1d1f21",
      },
    },
    {
      name: "Comment (.c)",
      scope: "comment",
      settings: {
        foreground: "#969896",
      },
    },
    {
      name: "Comment.Single (.c1)",
      scope: "comment.line",
      settings: {
        foreground: "#969896",
      },
    },
    {
      name: "Comment.Multiline (.cm)",
      scope: "comment.block",
      settings: {
        foreground: "#969896",
      },
    },
    {
      name: "Number (.m)",
      scope: "constant.numeric",
      settings: {
        foreground: "#de935f",
      },
    },
    {
      name: "String.Char (.sc)",
      scope: "constant.character",
      settings: {
        foreground: "#c5c8c6",
      },
    },
    {
      name: "String.Escape (.se)",
      scope: "constant.character.escape",
      settings: {
        foreground: "#de935f",
      },
    },
    {
      name: "Keyword.Constant (.kc)",
      scope: "constant.language",
      settings: {
        foreground: "#b294bb",
      },
    },
    {
      name: "Name (.n)",
      scope: "entity.name",
      settings: {
        foreground: "#c5c8c6",
      },
    },
    {
      name: "Name.Function (.nf)",
      scope: "entity.name.function",
      settings: {
        foreground: "#81a2be",
      },
    },
    {
      name: "Name.Tag (.nt)",
      scope: "entity.name.tag",
      settings: {
        foreground: "#8abeb7",
      },
    },
    {
      name: "Name.Attribute (.na)",
      scope: "entity.other.attribute-name",
      settings: {
        foreground: "#81a2be",
      },
    },
    {
      name: "Keyword (.k)",
      scope: "keyword",
      settings: {
        foreground: "#b294bb",
      },
    },
    {
      name: "Operator (.o)",
      scope: "keyword.operator",
      settings: {
        foreground: "#8abeb7",
      },
    },
    {
      name: "Generic.Strong (.gs)",
      scope: "markup.bold",
      settings: {
        fontStyle: "bold",
      },
    },
    {
      name: "Generic.Heading (.gh)",
      scope: "markup.heading",
      settings: {
        foreground: "#c5c8c6",
        fontStyle: "bold",
      },
    },
    {
      name: "Generic.Subheading (.gu)",
      scope: "markup.heading.2",
      settings: {
        foreground: "#8abeb7",
        fontStyle: "bold",
      },
    },
    {
      name: "Generic.Emph (.ge)",
      scope: "markup.italic",
      settings: {
        fontStyle: "italic",
      },
    },
    {
      name: "Keyword.Declaration (.kd)",
      scope: "storage.type",
      settings: {
        foreground: "#b294bb",
      },
    },
    {
      name: "String (.s)",
      scope: "string",
      settings: {
        foreground: "#b5bd68",
      },
    },
    {
      name: "String.Single (.s1)",
      scope: "string.quoted.single",
      settings: {
        foreground: "#b5bd68",
      },
    },
    {
      name: "String.Double (.s2)",
      scope: "string.quoted.double",
      settings: {
        foreground: "#b5bd68",
      },
    },
    {
      name: "String.Heredoc (.sh)",
      scope: "string.unquoted",
      settings: {
        foreground: "#b5bd68",
      },
    },
    {
      name: "String.Interpol (.si)",
      scope: "string.interpolated",
      settings: {
        foreground: "#de935f",
      },
    },
    {
      name: "String.Regex (.sr)",
      scope: "string.regexp",
      settings: {
        foreground: "#b5bd68",
      },
    },
    {
      name: "String.Other (.sx)",
      scope: "string.other",
      settings: {
        foreground: "#b5bd68",
      },
    },
    {
      name: "Name.Variable (.nv)",
      scope: "variable",
      settings: {
        foreground: "#cc6666",
      },
    },
    {
      name: "Name.Builtin (.nb)",
      scope: "variable.language",
      settings: {
        foreground: "#c5c8c6",
      },
    },
  ],
  fg: "#c5c8c6",
  bg: "#1d1f21",
};

export const extraThemes = ["tomorrow-night"];
