export interface ShikiPygmentsTheme {
  name: string;
  type: "light" | "dark";
  settings: {
    name?: string;
    scope?: string;
    settings: {
      foreground?: string;
      background?: string;
      fontStyle?: string;
    };
  }[];
  fg: string;
  bg: string;
}

export const tomorrowNight: ShikiPygmentsTheme;

export type ExtraTheme = "tomorrow-night";
export const extraThemes: ExtraTheme[];
