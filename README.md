# shiki-pygments-themes

Extra themes for [Shiki](https://github.com/shikijs/shiki) based on [Pygments](https://pygments.org/) styles.

- [Source code](https://codeberg.org/joaopalmeiro/shiki-pygments-themes)
- [npm package](https://www.npmjs.com/package/shiki-pygments-themes)
- [Documentation](https://tsdocs.dev/docs/shiki-pygments-themes/0.0.1)

## Available themes

| Theme                        | Source                                                                                                               | Version                                  |
| ---------------------------- | -------------------------------------------------------------------------------------------------------------------- | ---------------------------------------- |
| [`tomorrow-night`](index.js) | [Tomorrow Night](https://github.com/MozMorris/tomorrow-pygments) by [Chris Kempson](https://github.com/chriskempson) | c6ca1a308e7e93cb18a54f93bf964f56e4d07acf |

## Development

Install [fnm](https://github.com/Schniz/fnm) (if necessary).

```bash
fnm install && fnm use && node --version
```

```bash
npm install
```

```bash
npm run lint
```

```bash
npm run format
```

```bash
npm pack --dry-run
```

## Deployment

```bash
npm version patch
```

```bash
npm version minor
```

```bash
npm version major
```

- Update the package version in the _Documentation_ URL at the top of the README file.
- Commit and push changes.
- Create a tag on [GitHub Desktop](https://github.blog/2020-05-12-create-and-push-tags-in-the-latest-github-desktop-2-5-release/).
- Check [Codeberg](https://codeberg.org/joaopalmeiro/shiki-pygments-themes/tags).

```bash
npm login
```

```bash
npm publish
```

- Check [npm](https://www.npmjs.com/package/shiki-pygments-themes).
