# Notes

- Themes generated from the https://codeberg.org/joaopalmeiro/textmate-pygments repo
- https://gist.github.com/schmod/5104392
- https://macromates.com/manual/en/language_grammars#naming_conventions
- https://pygments.org/docs/tokens/
- https://github.com/pygments/pygments/blob/fcd64b60bfbe4a74529394e722c784b3547745a3/pygments/token.py#L120
- https://github.com/pygments/pygments/blob/2.17.2/pygments/styles/monokai.py
- https://github.com/pygments/pygments/blob/2.17.2/pygments/lexers/python.py#L248
- https://github.com/pygments/pygments/blob/2.17.2/pygments/lexers/python.py#L514
- https://github.com/pygments/pygments/blob/2.17.2/pygments/lexers/python.py#L900
- https://github.com/pygments/pygments/blob/2.17.2/pygments/lexers/python.py#L1057
- https://codeberg.org/joaopalmeiro/shiki-extra-themes
- https://gitlab.com/gitlab-org/gitlab/-/blob/95b0a2fb4dc7300b62af0a2665bc702a6794d863/app/assets/stylesheets/highlight/themes/dark.scss#L295
- https://github.com/davinche/pygments-from-tmtheme
- https://github.com/davinche/pygments-from-tmtheme/blob/a621872e0312747be48b43e889588b9bf999521b/tmTheme2pygment.py
- https://news.ycombinator.com/item?id=17932872
- https://github.com/trishume/syntect
- https://github.com/Speyll/veqev
- https://github.com/getzola/zola
- https://pandoc.org/MANUAL.html#syntax-highlighting
- https://www.garrickadenbuie.com/blog/pandoc-syntax-highlighting-examples/
- https://ycl6.github.io/Compare-pandoc-styles/
- https://github.com/jgm/skylighting
- https://github.com/alecthomas/chroma
- https://github.com/shikijs/shiki/blob/v0.14.6/packages/shiki/languages/python.tmLanguage.json
- https://github.com/shikijs/shiki/blob/v0.14.6/packages/shiki/languages/python.tmLanguage.json#L596 vs. https://github.com/pygments/pygments/blob/2.17.2/pygments/lexers/python.py#L248
- https://github.com/shikijs/shiki/blob/v0.14.6/packages/shiki/languages/python.tmLanguage.json#L1705 vs. https://github.com/pygments/pygments/blob/2.17.2/pygments/lexers/python.py#L256
- https://www.sublimetext.com/docs/scope_naming.html#punctuation
- https://biomejs.dev/internals/changelog/ + https://www.npmjs.com/package/@biomejs/biome

## Commands

```bash
pandoc --list-highlight-styles
```

```bash
pandoc --print-highlight-style pygments > my.theme
```

```bash
npm install -D @biomejs/biome sort-package-json
```

```bash
rm -rf node_modules/
```
